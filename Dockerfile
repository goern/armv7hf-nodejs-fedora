FROM docker.io/resin/armv7hf-fedora:latest

ENV NAME=nodejs \
    NPM_CONFIG_PREFIX=$HOME/.npm-global \
    APP_ROOT=/opt/app-root \
    HOME=/opt/app-root/src \
    PATH=/opt/app-root/src/bin:/opt/app-root/bin:$PATH \
    PATH=$HOME/node_modules/.bin/:$HOME/.npm-global/bin/:$PATH

ENV SUMMARY="For Node.js applications" \
    DESCRIPTION="Node.js available as docker container based on Fedora ARMv7"

LABEL summary="$SUMMARY" \ 
      description="$DESCRIPTION" \ 
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="$SUMMARY" \
      io.openshift.tags="fedora,armv7,nodejs,npm" \
      architecture=armv7l \
      name="goern/armv7hf-nodejs-fedora" \
      license="GPLv3"

RUN dnf install -y --nodocs nodejs git && \
    dnf clean all -y

WORKDIR ${HOME}

RUN mkdir /.npm-global && \
    useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin -c "Default Application User" default && \
    chown -R 1001:0 ${APP_ROOT} /.npm-global && \
    chmod -R ug+rwx ${APP_ROOT} /.npm-global

EXPOSE 8080

USER 1001
